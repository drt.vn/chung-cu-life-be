# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# (1..100).each do |i|
#   User.create(
#     name: "Diamond #{i}",
#     username: "diamond.user.#{i}",
#     email: "diamond.user.#{i}@gmail.com",
#     password: "test"
#   )
# end

# (1..100).each do |i|
#   Post.create(
#     user_id: 1,
#     description: "Description #{300 + i}",
#     content: "Content #{300 + i}"
#   )
# end

# (1..50).each do |i|
#   Comment.create(
#     user_id: i+2,
#     post_id: 5,
#     content: 'This is post comment by user id #{i+3}'
#   )
# end

# statusArr = ['buy', 'sell', 'delivery', 'expire', 'done']
# (1..100).each do |i|
#   order_params = ActionController::Parameters.new({
#     order: {
#       buyer_id: i % 3 + 1,
#       name: 'Alex Nguyen ' + i.to_s,
#       phone: '0937256662',
#       address: '206, Lô E, chung cư Bàu Cát 2, p10 Tân Bình, HCM',
#       delivery_date: 1632614400000,
#       detail: 'Sườn non 1kg
#       Gà 1 con
#       Cá nục 1 kg,
#       Cá ngừ 2kg',
#       seller_id: 103,
#       post_id: 28,
#       status: statusArr[rand(statusArr.count)]
#     }
#   })

#   permited = order_params.require(:order).permit(:buyer_id, :name, 
#     :phone, :address, :delivery_date, :detail, :seller_id, :post_id, :status)

#   Order::CreateCommand.perform!(permited)
# end

# Category.create([
#   { name: 'nha_dep', full_name: 'Nhà đẹp' },
#   { name: 'danh_gia', full_name: 'Đánh giá' },
#   { name: 'cho_cu_dan', full_name: 'Chợ cư dân' },
#   { name: 'cho_thue', full_name: 'Thuê, cho thuê' },
#   { name: 'mua_ban', full_name: 'Mua, bán căn hộ' },
# ])

# category_interests = []
# category_interests.push(
#   {
#     category_id: 1,
#     show_count: 2000,
#     click_rate: 0.25,
#     available: 1
#   },
#   {
#     category_id: 2,
#     show_count: 377,
#     click_rate: 0.22,
#     available: 1
#   },
#   {
#     category_id: 3,
#     show_count: 368,
#     click_rate: 0.21,
#     available: 0
#   },
# )

# UserInterest.create(
#   user_id: 2,
#   category_interests: category_interests
# )

