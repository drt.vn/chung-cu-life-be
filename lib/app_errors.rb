module AppErrors
  class BadRequest < StandardError
    def status
      400
    end
  end

  class Unauthorized < StandardError
    def status
      401
    end
  end

  class Forbidden < StandardError
    def status
      403
    end
  end

  class NotFound < StandardError
    def status
      404
    end
  end

  class InternalServerError < StandardError
    def status
      500
    end
  end
end
