require 'aws-sdk-s3'

class DigitalOceanSpacesClient

  def initialize
    @digital_ocean_client = Aws::S3::Client.new(
      access_key_id: Rails.application.credentials.digital_ocean[:access_key_id],
      secret_access_key: Rails.application.credentials.digital_ocean[:secret_access_key],
      endpoint: Rails.application.credentials.digital_ocean[:endpoint],
      region: Rails.application.credentials.digital_ocean[:region],
    )
  end

  def delete_file(url)
    return unless url

    uri = URI::parse(url)
    path = uri.path
    path[0] = ''

    return if path.length == 0

    @digital_ocean_client.delete_object({ 
      bucket: 'cclife',
      key: path
    })
  end
end
