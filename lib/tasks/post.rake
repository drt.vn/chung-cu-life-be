
namespace :post do

  # Chạy task mỗi 5 phút
  desc 'Generate posts'
  task generate: :environment do
    redis = Redis::Base.new

    # Category posts
    category_ids = Category.all.pluck(:id)
    categorized_post_ids_hash = Post.category_posts(category_ids)
    category_ids.each do |category_id|
      categorized_post_ids = categorized_post_ids_hash[category_id]
      if categorized_post_ids && categorized_post_ids.length > 0
        redis.call('delete_category_posts', category_id)
        redis.call('register_category_posts', category_id, categorized_post_ids)
      end
    end

    # Location posts
    location_post_ids = Post.location_posts

    redis.call('delete_location_posts')
    if location_post_ids.length > 0
      redis.call('register_location_posts', location_post_ids)
    end
  end

  desc 'Expire shown posts older than 7 days'
  task expire_shown: :environment do
    Task::ExpireShownPostService.call()
  end
end
