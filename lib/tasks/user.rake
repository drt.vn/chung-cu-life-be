
namespace :user do

  desc 'Cập nhật số điện thoại và địa chỉ từ đơn hàng'
  task update_phone_address: :environment do
    User::UpdatePhoneAddressService.call()
  end
end 
