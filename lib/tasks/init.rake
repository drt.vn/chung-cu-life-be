
namespace :init do
  desc "Register category id to redis"
  task category: :environment do
    redis = Redis::Base.new
    category_ids = Category.all.pluck(:id)
    redis.call('register_category_ids', category_ids)
  end
end
