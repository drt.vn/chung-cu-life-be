
namespace :order do

  # Chạy task mỗi 1 ngày
  desc 'Các đơn hàng chưa được cập nhật trạng thái hoàn thành \
    sẽ được chuyển sang trạng thái hết hạn'
  task expire: :environment do
    orders = Order.expire_orders
    orders.each do |order|
      order.status = 'expire'
      order.save!
    end
  end
end
