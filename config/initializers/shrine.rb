require "shrine"
require "shrine/storage/s3"

# loads Active Record integration
Shrine.plugin :activerecord

# enables retaining cached file across form redisplays
Shrine.plugin :cached_attachment_data 

# extracts metadata for assigned cached files
Shrine.plugin :restore_cached_data

Shrine.plugin :store_dimensions

Shrine.plugin :remote_url, max_size: 20*1024*1024

s3_options = { 
  endpoint: Rails.application.credentials.digital_ocean[:endpoint],
  bucket: Rails.application.credentials.digital_ocean[:bucket],
  region: Rails.application.credentials.digital_ocean[:region],
  access_key_id: Rails.application.credentials.digital_ocean[:access_key_id],
  secret_access_key: Rails.application.credentials.digital_ocean[:secret_access_key],
}
 
Shrine.storages = { 
  cache: Shrine::Storage::S3.new(prefix: "cache", **s3_options), # temporary 
  store: Shrine::Storage::S3.new(public: true, **s3_options), # permanent 
}
