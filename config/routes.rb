Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  scope :api, defaults: { format: :json } do
    post 'auth/login', to: 'auths#create'
    post 'auth/logout', to: 'auths#destroy'
    get 'auth/user', to: 'auth/users#show'

    get '/p/:slug', to: 'posts#show'

    namespace 'auth' do
      post 'facebook', to: 'facebook#create'
    end

    resources :users, only: [:show] do
      member do
        put :location, to: 'users/location#update'
        put :avatar, to: 'users/avatar#update'

        get 'bookmark-posts', to: 'users/bookmark_posts#index'
        post 'bookmark-posts', to: 'users/bookmark_posts#create'

        resources :posts, only: [:index, :destroy], param: :post_id,
          controller: 'users/posts'

        resources :follows, only: [:create, :index], controller: 'users/follows'

        resources :unfollows, only: [:create], controller: 'users/unfollows'
      end
    end

    resources :cities, only: [:index]

    resources :posts, only: [:create, :index, :show]

    resources :apartments, only: [:index]

    namespace 'post' do
      resources :love, only: [:create]

      delete 'love', to: 'love#destroy'

      resources :comments, only: [:create, :index]

      delete 'comment', to: 'comment#destroy'
    end

    namespace 'shop' do
      resources :orders, only: [:index, :update]
    end

    resources :orders, only: [:create, :index]

    get 'guideline', to: 'documents#guideline'

    get 'principles', to: 'documents#principles'
    
    get 'privacy-policy', to: 'documents#privacy_policy'
  end  
end

