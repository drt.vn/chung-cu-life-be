json.cache! [:post, post.id, post.cache_updated_at&.to_s(:number)], expires_in: 1.hour do
  json.extract! post, :id, :description, :content, :slug, :phone, :user_id, :category_id

  json.images post.mobile_images do |image|
    json.url image['url']
    json.width image['width']
    json.height image['height']
  end
end

json.authorName post.user.name
json.authorAvatar post.user.avatar_url
json.location post.location
json.lovesCount post.loves_count
json.commentsCount post.comments_count
json.loved post.loved
