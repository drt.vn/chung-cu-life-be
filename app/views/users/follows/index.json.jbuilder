json.userFollowings do
  json.array! @user_followings do |user_following|
    json.extract! user_following, :id, :name, :avatar_url
    json.avatar_url user_following.avatar_url
  end
end

json.totalPages @total_pages
