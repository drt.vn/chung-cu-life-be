json.user do
  json.(@user, :id, :name)
  json.followed @user.followed
  json.followingsCount @user.followings_count
  json.followersCount @user.followers_count
  json.avatar @user.avatar_url
end
