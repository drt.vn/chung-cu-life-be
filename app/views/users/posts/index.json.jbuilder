json.posts do
  json.array! @posts do |post|
    json.extract! post, :id, :description, :slug
    json.thumbnailUrl post.mobile_images ? post.mobile_images[0]['url'] : nil
  end
end

json.totalPages @total_pages
