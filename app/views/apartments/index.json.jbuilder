json.array! @apartments do |apartment|
  json.id apartment.id
  json.name apartment.name
  json.city apartment.city.name
  json.district apartment.district.name
end
