json.comments do
  json.array! @comments do |comment|
    json.extract! comment, :id, :content
    json.user do
      json.name comment.user.name
      json.avatar comment.user.avatar_url
    end
  end
end

json.totalPages @total_pages
