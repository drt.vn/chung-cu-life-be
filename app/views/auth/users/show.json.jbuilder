if @user
  json.user do
    json.(@user, :id, :email, :name, :phone, :address, :has_store)
    json.cityId @user.location&.city_id
    json.districtId @user.location&.district_id
    json.latitude @user.location&.latitude
    json.longitude @user.location&.longitude
    json.location_granted @user.location&.location_granted
    json.avatar @user.avatar_url
    json.followersCount @user.followers_count
    json.followingsCount @user.followings_count
  end
end
