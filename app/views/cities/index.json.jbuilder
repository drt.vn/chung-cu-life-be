json.cities  do
  json.array! @cities do |city|
    json.extract! city, :id, :name
  end
end

json.districts do
  @cities.each do |city|
    json.set! city.id do
      json.array! city.districts do |district|
        json.extract! district, :id, :name
      end
    end
  end 
end
