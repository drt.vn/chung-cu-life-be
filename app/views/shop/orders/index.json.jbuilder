json.orders do
  json.array! @orders do |order|
    json.extract! order, :id, :detail, :status
    json.buyerId order.buyer_id
    json.buyerAvatar order.buyer.avatar_url
    json.name order.name
    json.address order.address
    json.phone order.phone
    json.deliveryDate order.delivery_date&.to_i
    json.postId order.post_id
    json.sellerPhone order.post.phone
    json.thumbnailImage order.post && order.post.mobile_images[0]&.[]('url')
  end
end

json.totalPages @total_pages
json.totalItems @total_items

