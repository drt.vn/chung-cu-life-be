class Post::DeleteJob < ApplicationJob
  queue_as :default

  def perform(user_id, post_id)
    Post::DeleteCommand.perform!(user_id, post_id)
  end
end
