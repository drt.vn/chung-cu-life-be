class ProcessVariantsJob < ApplicationJob
  queue_as :default

  def perform(post_id, image_paths)
    return if (image_paths || []).length == 0
    post = Post.find(post_id)

    image_uploader = ImageUploader.new(:store)

    variant_urls = image_paths.map do |image_path|
      ImageProcessing::MiniMagick.source(image_path).
        resize_to_limit(500, nil).
        call(destination: image_path)
      
      temp_image = File.open(image_path, binmode: true)
      dev_prefix = Rails.env.production? ? '' : 'dev/'

      uploaded_file = image_uploader.upload(temp_image, 
        location: "#{dev_prefix}post/#{post_id}/#{SecureRandom.hex}")

      {
        url: uploaded_file.url,
        width: uploaded_file.metadata["width"] || 0,
        height: uploaded_file.metadata["height"] || 0,
      }
    end

    post.mobile_images = variant_urls
    post.save!
  end
end
