class UnlovePostJob < ApplicationJob
  queue_as :default

  def perform(user_id, post_id)
    Love.where(
      user_id: user_id,
      post_id: post_id
    ).delete_all

    post = Post.find(post_id)
    loves_count = post.loves_count - 1
    if (loves_count >= 0)
      post.update!(loves_count: loves_count)
    end
  end
end
