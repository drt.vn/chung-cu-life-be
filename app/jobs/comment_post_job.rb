class CommentPostJob < ApplicationJob
  queue_as :default

  def perform(user_id, post_id, content)
    Comment.create(
      user_id: user_id, 
      post_id: post_id,
      content: content
    )

    post = Post.find(post_id)
    comments_count = post.comments_count + 1
    post.update!(comments_count: comments_count)
  end
end
