class User::CreateBookmarkPostJob < ApplicationJob
  queue_as :default

  def perform(user_id, post_id)
    user_bookmark_post = UserBookmarkPost.find_by(user_id: user_id)

    if user_bookmark_post
      user_bookmark_post.bookmark_posts.unshift(post_id).uniq!
      user_bookmark_post.save!
    else
      bookmark_posts = [post_id]
      UserBookmarkPost.create(
        user_id: user_id,
        bookmark_posts: bookmark_posts      
      )
    end
  end
end
