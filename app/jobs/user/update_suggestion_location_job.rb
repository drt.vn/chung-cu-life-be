class User::UpdateSuggestionLocationJob < ApplicationJob

  queue_as :default

  def perform(user_id)
    User::GenerateSuggestionApartmentService.call(user_id)
    User::GenerateSuggestionLocationService.call(user_id)
  end
end
