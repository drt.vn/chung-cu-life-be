class User::UnfollowJob < ApplicationJob

  queue_as :default

  # Current user (source) UNfollow another user (destination)
  def perform(current_user_id, follow_user_id)
    src_user = User.find(current_user_id)
    des_user = User.find(follow_user_id)

    return unless src_user && des_user

    user_following = UserFollowing.find_by(user_id: current_user_id)
    following_ids = user_following&.following_ids
    if following_ids && following_ids.include?(follow_user_id)
      following_ids.delete(follow_user_id)
    end

    user_follower = UserFollower.find_by(user_id: follow_user_id)
    follower_ids = user_follower&.follower_ids
    if follower_ids && follower_ids.include?(current_user_id)
      follower_ids.delete(current_user_id)
    end

    src_user.followings_count -= 1
    des_user.followers_count -= 1

    ActiveRecord::Base.transaction do
      user_follower.save!
      user_following.save!
      src_user.save!
      des_user.save!
    end
  end
end