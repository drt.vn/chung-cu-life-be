class User::UpdateCityJob < ApplicationJob

  queue_as :default

  def perform(user_id, remote_id)
    return unless remote_id
    User::UpdateCityService.call(user_id, remote_id)
  end
end
