class User::FollowJob < ApplicationJob

  queue_as :default

  # Current user (source) follow another user (destination)
  def perform(current_user_id, follow_user_id)
    User::FollowCommand.perform!(current_user_id, follow_user_id)
  end
end