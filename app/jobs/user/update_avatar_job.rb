class User::UpdateAvatarJob < ApplicationJob
  queue_as :default

  def perform(user_id, avatar)
    return unless avatar

    ImageProcessing::MiniMagick.source(avatar).resize_to_limit(500, nil).call(destination: avatar)

    user = User.find(user_id)

    image_uploader = ImageUploader.new(:store)

    temp_image = File.open(avatar, binmode: true)
    dev_prefix = Rails.env.production? ? '' : 'dev/'

    uploaded_file = image_uploader.upload(temp_image, 
      location: "#{dev_prefix}user/#{user_id}/#{SecureRandom.hex}")

    old_avatar_url = user.avatar_url
    user.avatar_data = {
      url: uploaded_file.url,
      width: uploaded_file.metadata["width"] || 0,
      height: uploaded_file.metadata["height"] || 0,
    }
    user.save!

    # Remove old avatar
    DigitalOceanSpacesClient.new.delete_file(old_avatar_url)
  end
end
