class LovePostJob < ApplicationJob
  queue_as :default

  def perform(user_id, post_id)
    love = Love.find_by(
      user_id: user_id,
      post_id: post_id
    )

    return if love

    Love.create!(
      user_id: user_id,
      post_id: post_id
    )

    post = Post.find(post_id)
    loves_count = post.loves_count + 1
    post.update!(loves_count: loves_count)
  end
end
