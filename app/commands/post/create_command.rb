class Post::CreateCommand < Command

  COMMON_VALUE = 5

  def initialize(current_user_id, post_params)
    @current_user_id = current_user_id
    @post_params = post_params
  end

  def perform!
    return unless @current_user_id

    post = Post.new(
      user_id: @current_user_id,
      description: @post_params[:description],
      category_id: @post_params[:category_id].to_i > 0 ? @post_params[:category_id] : nil,
      phone: @post_params[:phone],
      has_location: @post_params[:has_location] || false,
      is_resident: @post_params[:is_resident],
      apartment_id: @post_params[:apartment_id].to_i > 0 ? @post_params[:apartment_id] : nil,
      city_id: @post_params[:city_id].to_i > 0 ? @post_params[:city_id] : nil,
      district_id: @post_params[:district_id].to_i > 0 ? @post_params[:district_id] : nil ,
      content: @post_params[:content],
      value: COMMON_VALUE,
    )

    images = @post_params[:images] && @post_params[:images].values || []
    image_paths = images.map do |image|
      image.tempfile.path
    end

    post.save!
    ProcessVariantsJob.perform_later(post.id, image_paths)
    post
  end
end
