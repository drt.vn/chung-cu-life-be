require 'uri'

class Post::DeleteCommand < Command

  def initialize(user_id, post_id)
    @user_id = user_id
    @post_id = post_id
  end

  def perform!
    post = Post.find(@post_id)
    return unless post && post.user_id == @user_id

    Love.where(post_id: @post_id).destroy_all
    Comment.where(post_id: @post_id).destroy_all

    # Remove images
    do_client = DigitalOceanSpacesClient.new
    post.mobile_images.each do |image|
      do_client.delete_file(image['url'])
    end

    post.destroy
  end
end
