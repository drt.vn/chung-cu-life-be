class Order::CreateCommand < Command

  def initialize(order_params)
    @order_params = order_params
  end

  def perform!
    if @order_params[:delivery_date]
      @order_params[:delivery_date] = Time.at(@order_params[:delivery_date] / 1000)
    end
    @order_params[:status] = 'buy'
    Order.create!(@order_params)
  end
end
