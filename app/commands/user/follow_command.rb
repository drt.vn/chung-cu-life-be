class User::FollowCommand < Command

  # Current user (source) follow another user (destination)
  def initialize(current_user_id, follow_user_id)
    @current_user_id = current_user_id
    @follow_user_id = follow_user_id
  end

  def perform!
    src_user = User.find(@current_user_id)
    des_user = User.find(@follow_user_id)

    return unless src_user && des_user

    user_following = UserFollowing.find_or_create_by(user_id: @current_user_id)
    user_following.following_ids ||= []
    following_ids = user_following.following_ids
    return if following_ids.include?(@follow_user_id)
    following_ids.unshift(@follow_user_id)

    user_follower = UserFollower.find_or_create_by(user_id: @follow_user_id)
    user_follower.follower_ids ||= []
    follower_ids = user_follower.follower_ids
    return if follower_ids.include?(@current_user_id)
    follower_ids.unshift(@current_user_id)
    
    src_user.followings_count += 1 
    des_user.followers_count += 1

    ActiveRecord::Base.transaction do
      user_follower.save!
      user_following.save!
      src_user.save!
      des_user.save!
    end
  end
end
