class Post::CommentsController < ApplicationController
  skip_before_action :authenticate_user, except: :create

  def create
    CommentPostJob.perform_later(
      current_user.id, 
      params[:post_id],
      params[:content]
    )

    render json: { status: :ok }
  end

  def destroy
    DeletePostCommentJob.perform_later(
      current_user.id, 
      params[:post_id]
    )
  end

  def index
    @pagy, @comments = pagy(Comment.includes(:user).where(post_id: params[:post_id])
      .order(created_at: :desc), page: params[:page], items: 20)
    @total_pages = pagy_metadata(@pagy)[:pages]
  end
end
