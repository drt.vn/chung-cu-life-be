class Post::LoveController < ApplicationController

  def create
    LovePostJob.perform_later(
      current_user.id, 
      params[:post_id]
    )
  end

  def destroy
    UnlovePostJob.perform_later(
      current_user.id, 
      params[:post_id]
    )
  end
end
