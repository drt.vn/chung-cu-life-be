class ApartmentsController < ApplicationController

  def index
    @apartments = Apartment.search(params[:name]).includes(:city, :district).limit(50)
  end
end
