class DocumentsController < ApplicationController
  skip_before_action :authenticate_user

  def guideline
    text = File.read(Rails.root.join("document/guideline.html"))
    render json: { text: text }, status: :ok
  end

  def principles
    text = File.read(Rails.root.join("document/principles.html"))
    render json: { text: text }, status: :ok
  end

  def privacy_policy
    text = File.read(Rails.root.join("document/privacy-policy.html"))
    render json: { text: text }, status: :ok
  end

end
