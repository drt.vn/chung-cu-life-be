class Auth::FacebookController < ApplicationController
  skip_before_action :authenticate_user
  before_action :authenticate

  def create
    @remote_ip = params[:client_ip]
    User::UpdateCityJob.perform_later(@entity.id, @remote_ip)
    render json: { accessToken: auth_token }, status: :created
  end

  private

  def authenticate
    unless entity.present?
      raise NotAuthorizedException
    end
  end

  def auth_token
    JwtService.encode({
      user_id: @entity.id,
      exp: 365.days.from_now.to_i
    })
  end

  def entity
    access_token = params[:access_token].split(' ')[1]
    @entity ||=
      if FacebookService.valid_token?(access_token)
        data = FacebookService.fetch_data(access_token)
        user = User.find_or_create_by uid: data['id'] do |user|
          user.name = data['name']
          user.email = data['email']
          user.provider = 'facebook'
          user.password = SecureRandom.hex
        end

        user
      end
  end
end
