class Auth::UsersController < ApplicationController
  def show
    @user = User.includes(:location).find(@current_user.id)
  end
end
