class OrdersController < ApplicationController
  skip_before_action :authenticate_user, except: :index

  def create
    Order::CreateCommand.perform!(order_params)
    render json: {}, status: :ok
  end

  def index
    @pagy, @orders = pagy(Order.orders(current_user&.id),
      page: params[:page],
      items: 15)

    @total_pages = pagy_metadata(@pagy)[:pages]
  end

  private

  def order_params
    params.require(:order).permit(:buyer_id, :name, 
      :phone, :address, :delivery_date, :detail, :seller_id, :post_id)
  end
end
