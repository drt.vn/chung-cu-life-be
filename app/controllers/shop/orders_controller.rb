class Shop::OrdersController < ApplicationController

  def index
    date = params[:date] ? 
      Date.parse(Time.at(params[:date].to_i / 1000).to_s) : 
      Date.today

    @pagy, @orders = pagy(Order.includes(:post, :buyer)
      .where(seller_id: current_user&.id)
      .where('DATE(delivery_date) = ?', date)
      .order(delivery_date: :desc), 
      page: params[:page], 
      items: 30)

    @total_pages = pagy_metadata(@pagy)[:pages]
    @total_items = pagy_metadata(@pagy)[:count]
  end

  def update
    order = Order.find(params[:id])
    if current_user.id == order.seller_id
      order.status = params[:status]
      order.save!
    end
    render json: {}, status: :ok
  end
end
