class ApplicationController < ActionController::API
  include ActionController::MimeResponds
  include ActionController::Caching
  include TokenAuthenticatable
  helper_method :current_user
  include Pagy::Backend
  include ErrorHandler

  before_action :setup_redis

  attr_reader :redis

  respond_to :json

  def throw_bad_request!
    raise AppErrors::BadRequest
  end

  def throw_not_found!
    raise AppErrors::NotFound
  end

  def throw_internal_error!
    raise AppErrors::InternalServerError
  end

  def throw_forbidden!
    raise AppErrors::Forbidden
  end

  private

  def setup_redis
    @redis = Redis::Base.new
  end
end
