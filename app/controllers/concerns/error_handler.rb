module ErrorHandler
  extend ActiveSupport::Concern

  ERROR_MESSAGE_ERROR_OCCURRED              = 'Có lỗi xảy ra!'.freeze
  ERROR_MESSAGE_UNAUTHORIZED_ERROR_OCCURRED = 'Lỗi đăng nhập!'.freeze
  ERROR_MESSAGE_ERROR_5xx_OCCURRED          = 'Lỗi server, xin thử lại sau!'.freeze

  included do
    unless Rails.env.development?
      rescue_from Exception,
                  AppErrors::InternalServerError, with: :response_5xx
    end
    rescue_from AppErrors::BadRequest, with: :response_400
    rescue_from AppErrors::Unauthorized, with: :response_401
    rescue_from AppErrors::Forbidden, with: :response_403
    rescue_from AppErrors::NotFound, with: :response_404

  end

  def response_400(exception)
    render json: error_response(exception, ERROR_MESSAGE_ERROR_OCCURRED), status: :bad_request, layout: false
  end

  def response_401(exception)
    render json: error_response(exception, ERROR_MESSAGE_UNAUTHORIZED_ERROR_OCCURRED),
           status: :unauthorized, layout: false
  end

  def response_403(exception)
    render json: error_response(exception, ERROR_MESSAGE_ERROR_OCCURRED), status: :forbidden, layout: false
  end

  def response_404(exception)
    render json: error_response(exception, ERROR_MESSAGE_ERROR_OCCURRED), status: :not_found, layout: false
  end

  def response_5xx(exception)
    render json: error_response(exception, ERROR_MESSAGE_ERROR_5xx_OCCURRED),
           status: :internal_server_error, layout: false
  end

  private

  def error_response(exception, default_message, detail = 0)
    message = if exception.message == exception.class.to_s
                default_message
              else
                exception.message
              end
    { meta: { detail: detail, message: message } }.to_json
  end

end
