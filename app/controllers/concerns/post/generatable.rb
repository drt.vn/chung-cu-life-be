module Post::Generatable
  extend ActiveSupport::Concern

  included do
    attr_reader :generate_service

    before_action :start_generate_service, only: [:index]
  end

  private

  def start_generate_service
    @generate_service ||= Post::GenerateService.new(current_user&.id || request.headers["Remote-IP"])
  end
end
