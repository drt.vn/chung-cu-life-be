module TokenAuthenticatable
  extend ActiveSupport::Concern

  included do
    attr_reader :current_user

    before_action :authenticate_user
  
    def current_user
      @current_user = Auth::DecodeAuthenticationCommand.call(request.headers).result
      session[:current_user_id] = @current_user&.id
      @current_user
    end
  end

  private

  def authenticate_user
    current_user
    raise AppErrors::Unauthorized unless @current_user
  end
end
