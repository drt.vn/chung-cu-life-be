class Users::LocationController < ApplicationController

  def update
    @user_location = UserLocation.find_or_create_by(user_id: params[:user_id])

    if @user_location.update(user_location_params)
      User::UpdateSuggestionLocationJob.perform_later(@user_location.user_id)
      render json: { status: :ok, message: 'Success' }
    else
      render json: { error: @user_location.errors }, status: :unprocessable_entity
    end
  end

  private

  def user_location_params
    params.require(:location).permit(:user_id, :latitude, 
      :longitude, :location_granted)
  end
end
