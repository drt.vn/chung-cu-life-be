class Users::UnfollowsController < ApplicationController

  def create
    return if !current_user
    User::UnfollowJob.perform_later(current_user&.id, params[:id].to_i)
    render json: {}, status: :ok
  end
end
