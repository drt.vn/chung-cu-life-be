class Users::AvatarController < ApplicationController

  def update
    User::UpdateAvatarJob.perform_later(params[:id], params[:avatar].tempfile.path)
  end
end
