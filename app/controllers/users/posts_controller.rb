class Users::PostsController < ApplicationController
  skip_before_action :authenticate_user, only: [:index]

  def index
    @pagy, @posts = pagy(Post.where(user_id: params[:id]).order(created_at: :desc), 
      page: params[:page],
      items: 15)

    @total_pages = pagy_metadata(@pagy)[:pages]
  end

  def destroy
    user_id = params[:id]&.to_i
    if current_user&.id == user_id
      Post::DeleteJob.perform_later(user_id, params[:post_id])
    end

    render json: {}, status: :ok
  end
end
