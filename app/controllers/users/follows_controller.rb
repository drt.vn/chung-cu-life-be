class Users::FollowsController < ApplicationController

  USER_FOLLOWINGS_PER_PAGE = 30

  def create
    return if !current_user
    User::FollowJob.perform_later(current_user&.id, params[:id].to_i)
    render json: {}, status: :ok
  end

  def index
    user_following_ids = UserFollowing.find_by(user_id: params[:id])&.following_ids
    return unless user_following_ids

    page = params[:page]&.to_i || 1
    page_user_following_ids = user_following_ids.slice((page - 1) * USER_FOLLOWINGS_PER_PAGE, USER_FOLLOWINGS_PER_PAGE)
    
    sanitized_query = ActiveRecord::Base.send(:sanitize_sql_array, ["field(id, ?)",          page_user_following_ids])
    @user_followings = User.where(id: page_user_following_ids).
      order(Arel.sql(sanitized_query))

    pages = user_following_ids.length / USER_FOLLOWINGS_PER_PAGE
    pages = user_following_ids.length > pages * USER_FOLLOWINGS_PER_PAGE ? pages + 1 : pages
    @total_pages = pages
  end
end
