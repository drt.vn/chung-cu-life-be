class Users::BookmarkPostsController < ApplicationController

  ARR_ITEMS_PER_PAGE = 15

  def create
    User::CreateBookmarkPostJob.perform_later(
      params[:id],
      params[:post_id]
    )

    render json: {}, status: :ok
  end

  def index
    user_bookmark_posts = UserBookmarkPost.find_by(user_id: params[:id])&.bookmark_posts
  
    if !user_bookmark_posts || user_bookmark_posts.length == 0
      @posts = []
      render 'users/posts/index'
      return
    end
    
    page = params[:page].to_i
    page_post_ids = user_bookmark_posts.slice((page-1) * ARR_ITEMS_PER_PAGE, ARR_ITEMS_PER_PAGE)

    sanitized_query = ActiveRecord::Base.send(:sanitize_sql_array, ["field(id, ?)",    page_post_ids])
    @posts = Post.where(id: page_post_ids).order(Arel.sql(sanitized_query))
    pages = user_bookmark_posts.length / ARR_ITEMS_PER_PAGE
    pages = user_bookmark_posts.length > pages * ARR_ITEMS_PER_PAGE ? pages + 1 : pages
    @total_pages = pages

    render 'users/posts/index'
  end
end
