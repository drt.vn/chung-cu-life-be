class CitiesController < ApplicationController

  def index
    @cities = City.includes(:districts).all
  end
end
