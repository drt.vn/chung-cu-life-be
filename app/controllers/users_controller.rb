class UsersController < ApplicationController
  skip_before_action :authenticate_user

  def show
    @user = User.find(params[:id])
    unless @user
      render json: {}, status: :not_found 
      return
    end

    user_following = UserFollowing.find_by(user_id: current_user&.id)
    following_ids = user_following&.following_ids
    if following_ids && following_ids.include?(@user.id)
      @user.followed = true
    end
  end
end
