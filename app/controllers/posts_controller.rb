class PostsController < ApplicationController
  skip_before_action :authenticate_user, except: :create

  include Post::Generatable

  def create
    @post = Post::CreateCommand.perform!(current_user&.id, post_params)
    
    unless @post
      render json: { errors: post.errors }, status: :unprocessable_entity
    end
  end

  def show
    @post = params[:id] ? Post.find(params[:id]) :
      Post.find_by(slug: params[:slug]) 

    throw_not_found! unless @post
  end

  def index
    @posts = generate_service.call
  end

  def post_params
    params.permit(:description, :category_id, :content, :format,
    :has_location, :apartment_id, :is_resident, :phone, :city_id, :district_id, images: {})
  end
end
