class User::LoadRedisSuggestionPostService < ApplicationService

  def initialize(user_id)
    super()
    @user_id = user_id
  end

  def call
    # TODO Refresh key hàng giờ

    location_post_ids = redis.call('user_location_posts', @user_id)
    return location_post_ids if location_post_ids.length > 0

    # Lấy danh sách chung cư gần vị trí user đã tính toán sẵn từ DB
    user_suggestion_location = UserSuggestionLocation.find_by(user_id: @user_id)
    suggestion_apartment_ids = user_suggestion_location&.apartments

    # Lấy danh sách quận gần vị trí user đã tính toán sẵn từ DB
    suggestion_district_ids = user_suggestion_location&.districts
    target_post_ids = redis.call(:location_posts)

    # Trường hợp không lấy được suggestion_apartment_ids
    suggestion_location_post_ids = suggestion_apartment_ids ?
      Post.user_suggestion_location_posts(target_post_ids, suggestion_apartment_ids) :
      target_post_ids

    suggestion_district_post_ids = suggestion_district_ids ?
      Post.user_suggestion_district_posts(target_post_ids, suggestion_district_ids) :
      target_post_ids

    location_post_ids = merge_orderly(suggestion_location_post_ids, 
      suggestion_district_post_ids)

    return [] if location_post_ids.length == 0
 
    redis.call('register_user_location_posts', @user_id, location_post_ids)

    # Cập nhật thông tin mới mỗi 10 phút
    redis.call('expire_user_location_posts', @user_id)

    # Return
    redis.call('user_location_posts', @user_id)
  end

  def merge_orderly(first_post_ids, second_post_ids)
    merged_post_ids = []
    while (first_post_ids.length > 0 || second_post_ids.length > 0) do
      merged_post_ids.push(first_post_ids.shift)
      merged_post_ids.push(second_post_ids.shift)
    end

    merged_post_ids.compact!
    merged_post_ids.uniq!

    merged_post_ids
  end
end
