class User::GenerateSuggestionLocationService < ApplicationService

  def initialize(user_id)
    @user_location = UserLocation.find_by(user_id: user_id)
  end

  def call
    return unless @user_location.latitude && @user_location.longitude && @user_location.city_id

    sorted_suggestion_districts = []
    districts = CityDistrict.search_by_city(@user_location.city_id)
    districts.each do |district|
      district.distance = Geocoder::Calculations.distance_between([@user_location.latitude, @user_location.longitude], 
        [district.latitude, district.longitude])
    end

    sorted_districts = districts.sort_by(&:distance)

    user_suggestion_location = UserSuggestionLocation.find_or_create_by(user_id: @user_location.user_id)
    user_suggestion_location.update!(districts: sorted_districts.map(&:id).first(100))
  end
end
