class User::UpdatePhoneAddressService < ApplicationService

  def call
    orders = Order.user_update_orders
    orders.each do |order|
      user = order.buyer
      next if user.phone && user.address

      user.phone = order.phone

      found_address = User::UpdateLocationService.call(order.address, user.id)
      if found_address
        user.address = order.address
      end

      user.save!
    end
  end
end
