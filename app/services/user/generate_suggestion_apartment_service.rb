class User::GenerateSuggestionApartmentService < ApplicationService

  def initialize(user_id)
    @user_location = UserLocation.find_by(user_id: user_id)
  end

  def call
    return unless @user_location.latitude && @user_location.longitude && @user_location.city_id

    sorted_suggestion_apartments = []
    apartments = Apartment.search_by_city(@user_location.city_id)
    apartments.each do |apartment|
      apartment.distance = Geocoder::Calculations.distance_between([@user_location.latitude, @user_location.longitude], 
        [apartment.latitude, apartment.longitude])
    end

    sorted_apartments = apartments.sort_by(&:distance)

    user_suggestion_location = UserSuggestionLocation.find_or_create_by(user_id: @user_location.user_id)
    user_suggestion_location.update!(apartments: sorted_apartments.map(&:id).first(100))
  end
end
