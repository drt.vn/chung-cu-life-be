class User::UpdateLocationService < ApplicationService

  def initialize(address, user_id)
    @address = address
    @user_id = user_id
  end

  def call
    return unless @address

    # Tìm kiếm thông tin thành phố
    location = Geocoder.search(@address, params: {region: "vi"}).first
    return unless location
    
    # Cập nhật thành phố
    user_location = UserLocation.find_or_create_by(user_id: @user_id)
    city_id = Location::RetrieveCityIdService.call(location.state)
    if city_id && !user_location.city_id
      user_location.city_id = city_id
    end
    
    # Cập nhật quận, huyện
    district_id = Location::RetrieveDistrictIdService.call(location.city)
    if district_id && !user_location.district_id
      user_location.district_id = district_id
    end

    # Cập nhật toạ độ
    if location.coordinates[0] && !user_location.latitude
      user_location.latitude = location.coordinates[0]
    end

    if location.coordinates[1] && !user_location.longitude
      user_location.longitude = location.coordinates[1]
    end
    
    user_location.save!
    true
  end
end
