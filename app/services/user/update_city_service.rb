class User::UpdateCityService < ApplicationService

  def initialize(user_id, remote_ip)
    @user_id = user_id
    @remote_ip = remote_ip
  end

  def call
    city = Geocoder.search(@remote_ip).first&.region
    return unless city

    city_id = City.search_by_name(city).first&.id
    return unless city_id
    
    user_location = UserLocation.find_or_create_by(user_id: @user_id)
    user_location.update!(city_id: city_id)
  end
end
