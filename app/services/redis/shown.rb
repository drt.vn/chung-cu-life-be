module Redis::Shown

  USER_SHOWN_POST_IDS = 'user:%s:shown'

  def shown_posts(user_id)
    result = @redis.get(USER_SHOWN_POST_IDS % user_id)
    result ? JSON.parse(result) : []
  end

  def shown_posts_by(key)
    result = @redis.get(key)
    result ? JSON.parse(result) : []
  end

  def push_shown_posts(user_id, post_ids)
    shown = shown_posts(user_id)
    concat_posts = shown.concat(post_ids)

    sort_concat_posts = concat_posts.sort_by{ |a| a }.reverse

    @redis.set(USER_SHOWN_POST_IDS % user_id, sort_concat_posts.to_json)
  end

  def register_shown_posts(key, post_ids)
    @redis.set(key, post_ids.to_json)
  end
end
