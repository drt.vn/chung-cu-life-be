class Redis::Base
  include Redis::Category
  include Redis::Location
  include Redis::Shown
  include Redis::Scan
  
  def initialize
    @redis ||= Redis.current
  end

  # Rescue from redis connection errors in one place
  def call(method_name, *args)
    send(method_name, *args)
  rescue Redis::BaseError => e
    Rails.logger.warn("Redis error: CclifeRedis.#{method_name}(#{args}) → #{e.message}")
    nil
  end
end
