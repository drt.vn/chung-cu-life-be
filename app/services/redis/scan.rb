module Redis::Scan

  SCAN_USER_SHOWN = 'user:*:shown'

  def scan_user_shown(cursor='0')
    @redis.scan(cursor, match: SCAN_USER_SHOWN)
  end
end
