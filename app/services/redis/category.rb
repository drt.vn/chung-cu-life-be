module Redis::Category

  USER_GENERATED_POSTS = 'user:%s:generator'
  CATEGORY_POST_IDS = 'category:%s'

  def generated_posts(user_id)
    result = @redis.get(USER_GENERATED_POSTS % user_id)
    result ? JSON.parse(result) : []
  end

  def delete_generated_posts(user_id)
    @redis.del(USER_GENERATED_POSTS % user_id)
  end

  def register_generated_posts(user_id, post_ids)
    @redis.set(USER_GENERATED_POSTS % user_id, post_ids.to_json)
    @redis.expire(USER_GENERATED_POSTS % user_id, 1.hour)
  end

  def category_ids
    result = @redis.get('category_ids')
    result ? JSON.parse(result) : []
  end

  def register_category_ids(category_ids)
    @redis.set('category_ids', category_ids.to_json)
  end

  def category_posts(category_id)
    result = @redis.get(CATEGORY_POST_IDS % category_id)
    result ? JSON.parse(result) : []
  end

  def delete_category_posts(category_id)
    @redis.del(CATEGORY_POST_IDS % category_id)
  end

  def register_category_posts(category_id, post_ids)
    @redis.set(CATEGORY_POST_IDS % category_id, post_ids.to_json)
  end
end
