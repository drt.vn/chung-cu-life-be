module Redis::Location

  LOCATION_POST_IDS = 'location_post_ids'

  # Located posts by user
  USER_LOCATION_POST_IDS = 'user:%s:location_post_ids'

  def location_posts
    result = @redis.get(LOCATION_POST_IDS)
    result ? JSON.parse(result) : []
  end

  def delete_location_posts
    @redis.del(LOCATION_POST_IDS)
  end

  def register_location_posts(post_ids)
    @redis.set(LOCATION_POST_IDS, post_ids.to_json)
  end

  def user_location_posts(user_id)
    result = @redis.get(USER_LOCATION_POST_IDS % user_id)
    result ? JSON.parse(result) : []
  end

  def expire_user_location_posts(user_id)
    @redis.expire(USER_LOCATION_POST_IDS % user_id, 10.minutes)
  end

  def register_user_location_posts(user_id, post_ids)
    @redis.set(USER_LOCATION_POST_IDS % user_id, post_ids.to_json)
  end

end
