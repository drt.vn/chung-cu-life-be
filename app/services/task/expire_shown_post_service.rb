class Task::ExpireShownPostService < ApplicationService

  def call
    @redis = Redis::Base.new
    @all_posts = Post.active_posts
    
    next_cursor = '0'

    loop do
      next_cursor, result = @redis.call('scan_user_shown', next_cursor)
      break if result.length == 0 || next_cursor === '0'
      execute(result) 
    end
  end

  def execute(result)
    
    result.each do |key|
      shown_posts = @redis.call('shown_posts_by', key)
      new_shown_posts = shown_posts & @all_posts
      @redis.call('register_shown_posts', key, new_shown_posts)
    end
  end
end
