class Post::UpdateShownPostsService < ApplicationService

  def initialize(current_user_id, post_ids)
    super()
    @current_user_id = current_user_id
    @post_ids = post_ids
  end

  def call
    return if @post_ids.length == 0
    redis.call('push_shown_posts', @current_user_id, @post_ids)
  end
end
