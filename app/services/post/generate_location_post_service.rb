class Post::GenerateLocationPostService < ApplicationService

  NUM_OF_LOCATION_POSTS = 2

  def initialize(user_id)
    super()
    @user_id = user_id
  end

  def call
    location_post_ids = User::LoadRedisSuggestionPostService.call(@user_id)
    
    shown_post_ids = redis.call(:shown_posts, @user_id)
    target_post_ids = location_post_ids - shown_post_ids

    target_post_ids.first(NUM_OF_LOCATION_POSTS)
  end
end
