class Post::GenerateService < ApplicationService

  def initialize(current_user_id)
    super()
    @current_user_id = current_user_id
  end

  def call
    # Lấy 5 bài viết từ danh sách có sẵn được lưu trên Redis 
    generated_category_post_ids = 
      Post::GenerateCategoryPostService.call(@current_user_id) || 
      []

    # Lấy những bài viết thuộc chung cư khi user đăng nhập
    generated_location_post_ids = 
      if @current_user_id.instance_of?(Fixnum)
        Post::GenerateLocationPostService.call(@current_user_id)
      end

    generated_post_ids = generated_category_post_ids.union(generated_location_post_ids || [])

    if @current_user_id
      loved_post_ids = Love.where(user_id: @current_user_id).where(post_id: generated_post_ids).pluck(:post_id)
    end
    
    posts = Post.load_posts(generated_post_ids)
    posts.each do |post|
      post.loved = loved_post_ids ? loved_post_ids.include?(post.id) : false
    end

    # Make generated_post_ids to shown
    Post::UpdateShownPostsService.call(@current_user_id, generated_post_ids)

    posts
  end
end
