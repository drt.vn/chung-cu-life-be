class Post::GenerateCategoryPostService < ApplicationService

  NUM_OF_DELIVERY_POSTS = 5

  def initialize(current_user_id)
    super()
    @current_user_id = current_user_id
  end

  def call
    Post::GenerateCategoryPostFactoryService.call(@current_user_id)
    generated_post_ids = redis.call('generated_posts', @current_user_id)

    delivery_post_ids = generated_post_ids&.first(NUM_OF_DELIVERY_POSTS)
    remain_post_ids = generated_post_ids - delivery_post_ids

    redis.call('delete_generated_posts', @current_user_id)
    if (remain_post_ids.length > 0)
      redis.call('register_generated_posts', @current_user_id, remain_post_ids)
    end

    delivery_post_ids
  end
end
