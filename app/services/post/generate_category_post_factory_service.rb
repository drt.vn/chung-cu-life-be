class Post::GenerateCategoryPostFactoryService < ApplicationService

  # Kho bài viết có tối đa 100 bài
  NUM_OF_GENERATED_POSTS = 100
  NUM_OF_CATEGORIES = 10
  NUM_OF_CATEGORY_POSTS = 10
  NUM_OF_DELIVERY_POSTS = 5

  def initialize(current_user_id)
    super()
    @current_user_id = current_user_id
  end

  def call
    # category_interests = UserInterest.find_by(user_id: @current_user_id)&.
    #   category_interests

    @generated_post_ids = redis.call(:generated_posts, @current_user_id)

    # Số bài viết còn thì không cần lấy thêm bài viết
    return if @generated_post_ids && 
      @generated_post_ids.length >= NUM_OF_DELIVERY_POSTS * 2

    generating_post_ids = []

    category_ids = redis.call('category_ids')
    category_ids.each do | category_id |
      # Từ mỗi category lấy tối đa 10 bài viết
      category_post_ids = retrieve(category_id)
      generating_post_ids.concat(category_post_ids)

      # đến khi đủ 100 bài
      break if generating_post_ids.length >= NUM_OF_GENERATED_POSTS
    end

    generating_post_ids.first(NUM_OF_GENERATED_POSTS)

    generating_post_ids.shuffle
    if (generating_post_ids.length > 0)
      redis.call('register_generated_posts', @current_user_id, generating_post_ids)
    end
  end

  def retrieve(category_id)
    result = []

    # Get shown posts from Redis
    shown_category_post_ids = redis.call('shown_posts', @current_user_id)

    category_post_ids = redis.call('category_posts', category_id)
    return result if category_post_ids.length == 0
    total = category_post_ids.length

    i = 0
    startIndex = 0

    while result.length < NUM_OF_CATEGORY_POSTS && startIndex < total do
      # Lấy từng nhóm 10 bài viết
      startIndex = i * NUM_OF_CATEGORY_POSTS 
      group_posts = category_post_ids.slice(startIndex, NUM_OF_CATEGORY_POSTS)
      next unless group_posts

      # Lấy bài viết nếu chưa được hiển thị
      group_posts.each do |post_id|
        next if @generated_post_ids.include?(post_id)
        next if shown_category_post_ids.include?(post_id) 
        result.push(post_id) 
      end

      i = i + 1
    end 

    result.first(NUM_OF_CATEGORY_POSTS)
  end
end
