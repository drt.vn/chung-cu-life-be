class ApplicationService
  attr_reader :redis

  def initialize
    setup_redis
  end

  def self.call(*args, &block)
    new(*args, &block).call
  end

  private

  def setup_redis
    @redis = Redis::Base.new
  end
end
