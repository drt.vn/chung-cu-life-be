class Location::RetrieveDistrictIdService < ApplicationService

  def initialize(district_name)
    @district_name = district_name
  end

  def call
    @district_name.downcase!
    @district_name.slice!('thanh pho')
    @district_name.slice!('thành phố')
    @district_name.slice!('quận')
    @district_name.slice!('huyện')
    @district_name.slice!('huyen')
    @district_name.strip!

    CityDistrict.where('name like ?', @district_name).first&.id
  end
end
