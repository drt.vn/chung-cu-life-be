class Location::RetrieveCityIdService < ApplicationService

  def initialize(city_name)
    @city_name = city_name
  end

  def call
    @city_name.downcase!
    @city_name.slice!('thanh pho')
    @city_name.slice!('thành phố')
    @city_name.strip!

    City.where('name like ?', @city_name).first&.id
  end
end
