class City < ApplicationRecord
  has_many :districts, class_name: 'CityDistrict'

  scope :search_by_name, -> (name) {
    where('name like ?', "%#{name}%")
  }
end
