class CityDistrict < ApplicationRecord
  belongs_to :city

  attr_accessor :distance

  scope :search_by_city, -> (city_id) {
    where(city_id: city_id)
  }
end
