class Apartment < ApplicationRecord
  belongs_to :city
  belongs_to :district, class_name: 'CityDistrict'
  has_many :posts

  attr_accessor :distance

  scope :search, -> (name) {
    where('name like ?', "%#{name}%")
  }

  scope :search_by_city, -> (city_id) {
    where(city_id: city_id)
  }
end
