class Post < ApplicationRecord
  include Rails.application.routes.url_helpers

  has_many :loves, class_name: 'Love'
  belongs_to :apartment, optional: true
  belongs_to :category, optional: true
  belongs_to :city, optional: true
  belongs_to :district, class_name: 'CityDistrict', optional: true
  belongs_to :user

  attr_accessor :loved

  scope :latest_7_days, -> {
    where(:created_at => (Date.today - 300.days)..Time.now)
  }

  scope :is_public, -> { where(is_public: true) }

  validates :phone, 
    format: { 
      with: /\A0[1-9][0-9]{8,9}\z/,
      message: 'Số điện thoại không hợp lệ!'
    }, allow_blank: true
  validates :content, presence: true

  include Slugable
  include Post::Suggestable

  before_save do
    if description_changed? || content_changed? || mobile_images_changed?
      self.cache_updated_at = DateTime.now
    end
  end

  def location
    return apartment_id ? self.apartment.name : self.district&.name
  end
end
