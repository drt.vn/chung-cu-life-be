class Order < ApplicationRecord
  belongs_to :buyer, class_name: 'User',
    foreign_key: :buyer_id, optional: true
  belongs_to :seller, class_name: 'User',
    foreign_key: :seller_id, optional: true
  belongs_to :post, optional: true

  enum status: {
    register: 0, # Đăng ký mới
    buy: 1, # Người mua chốt mua
    sell: 2, # Người bán chốt bán
    delivery: 3, # Giao hàng
    expire: 4, # Hết hạn nếu quá ngày giao
    negotiate: 5, # 2 bên đang thương lượng, trao đổi thông tin
    done: 9, # Giao dịch hoàn thành
  }

  scope :orders, -> (user_id) {
    includes(:post).where(buyer_id: user_id)
      .order(delivery_date: :desc)
  }

  scope :expire_orders, -> (date=nil) {
    where('DATE(delivery_date) = ?', date || Date.yesterday).
      where.not(status: 'done')
  }

  scope :user_update_orders, -> (date=nil) {
    joins(:buyer).where('DATE(delivery_date) = ?', date || Date.yesterday).
      where("users.phone IS NULL OR users.address IS NULL")
  }
end
