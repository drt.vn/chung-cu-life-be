module Slugable
  extend ActiveSupport::Concern

  included do
    before_create :generate_slug
    before_update :generate_slug, if: :description_changed?
  end

  def generate_slug
    self.slug = to_slug
  end

  private

  def to_slug
    # Strip the string
    ret = self.description.strip
    ret.downcase!

    # blow away apostrophes
    ret.gsub! /['`]/, ""

    ret.gsub! /[áàảãạăắằẳẵặâấầẩẫậ]/, "a"
    ret.gsub! /[íìỉĩị]/, "i"
    ret.gsub! /[éèẻẽẹêếềểễệ]/, "e"
    ret.gsub! /[óòỏõọôốồổỗộơớờởỡợ]/, "o"
    ret.gsub! /[úùủũụưứừửữự]/, "u"
    ret.gsub! /[ýỳỷỹỵ]/, "y"
    ret.gsub! /[đ]/, "d"

    # @ --> at, and & --> and
    ret.gsub! /\s*@\s*/, " at "
    ret.gsub! /\s*&\s*/, " and "

    # replace all non alphanumeric, underscore or periods with underscore
    ret.gsub! /\s*[^A-Za-z0-9\.]\s*/, '-'  

    # Chang multiple `-` into one
    ret.gsub! /\-{2,}/, '-'

    # remove - at begin and end
    ret.gsub! /^\-+|\-+$/, ''

    # convert double underscores to single
    ret.gsub! /_+/, "-"

    # strip off leading/trailing underscore
    ret.gsub! /\A[_\.]+|[_\.]+\z/,""
    
    id_str = self.class.maximum(:id)&.next.to_s
    
    if id_str
      ret += '-' + id_str
    end

    ret
  end
end
