module Post::Suggestable
  extend ActiveSupport::Concern

  included do
    def self.category_posts(category_ids)
      self.latest_7_days.is_public.
      where(has_location: false).
      where(category_id: category_ids).
        order(value: :desc).
        pluck(:id, :category_id).
        group_by {
          |post| post[1]
        }.transform_values do |values|
          values.map{ |ele| ele[0] }
        end
    end

    def self.location_posts
      self.latest_7_days.is_public.
        where(has_location: true).order(updated_at: :desc).pluck(:id)
    end

    def self.active_posts
      self.latest_7_days.is_public.pluck(:id)
    end

    def self.user_suggestion_location_posts(target_post_ids, suggestion_apartment_ids)
      sanitized_query = ActiveRecord::Base.send(:sanitize_sql_array, ["field(apartment.id, ?)", suggestion_apartment_ids])
      includes(:apartment).
        where(id: target_post_ids).
        where(apartment: {id: suggestion_apartment_ids}).
        order(Arel.sql(sanitized_query)).
        pluck(:id)
    end

    def self.user_suggestion_district_posts(target_post_ids, suggestion_district_ids)
      sanitized_query = ActiveRecord::Base.send(:sanitize_sql_array, ["field(district_id, ?)", suggestion_district_ids])
      where(id: target_post_ids).
        where(district_id: suggestion_district_ids).
        order(Arel.sql(sanitized_query)).
        pluck(:id)
    end

    def self.load_posts(post_ids)
      where(id: post_ids).
        includes(:loves, :district, :apartment, :user).
        order(created_at: :desc)
    end
  end
end
