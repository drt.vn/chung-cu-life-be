class User < ApplicationRecord
  has_secure_password
  include Rails.application.routes.url_helpers

  attr_accessor :followed

  has_one :location, class_name: 'UserLocation'
  has_one :bookmark_post, class_name: 'UserBookmarkPost'
  has_one :following, class_name: 'UserFollowing'
  has_one :follower, class_name: 'UserFollower'

  has_many :comments
  has_many :posts

  validates_presence_of :email
  validates_uniqueness_of :email, case_sensitive: false
  validates_format_of :email, with: /@/

  def avatar_url
    avatar_data ? avatar_data['url'] : ''
  end

end
