module PostsHelper
  def loved?(post_id)
    return current_user ? !Love.where(
      user_id: current_user.id,
      post_id: post_id
    ).empty? : false
  end
end
